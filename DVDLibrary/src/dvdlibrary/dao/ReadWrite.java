/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dvdlibrary.dao;

import dvdlibrary.operations.DVD;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ReadWrite {

    public ArrayList<DVD> load(String fileName) {
        ArrayList<DVD> tempLib = new ArrayList<>();

        String currentLine;
        String[] splitLine;
        try {
            Scanner file = new Scanner(new FileReader(fileName));

            while (file.hasNextLine()) {
                currentLine = file.nextLine();
                splitLine = currentLine.split("::");
                DVD temp = new DVD(splitLine[0],
                        splitLine[1], splitLine[2], splitLine[3],
                        splitLine[4], splitLine[5]);
                tempLib.add(temp);
            }
        } catch (FileNotFoundException e) {
            return null;
        } 
            return tempLib;
    }
    

    public boolean save(ArrayList<DVD> d, String fileName) throws FileNotFoundException {
        try {
            PrintWriter output = new PrintWriter(new FileWriter(fileName));
            String outString;
            DVD current;
            Iterator<DVD> iter = d.iterator();
            while (iter.hasNext()) {
                current = iter.next();
                outString = current.getTitle() + "::"
                        + current.getYear() + "::"
                        + current.getRating() + "::"
                        + current.getDirName() + "::"
                        + current.getUserNote() + "::"
                        + current.getRuntime();
                output.println(outString);
                outString = "";
            }
            output.flush();
            output.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

}
