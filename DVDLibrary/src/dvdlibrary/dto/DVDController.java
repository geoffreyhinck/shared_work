/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dvdlibrary.dto;

import dvdlibrary.dao.ReadWrite;
import dvdlibrary.operations.DVD;
import dvdlibrary.operations.DVDLibrary;
import dvdlibrary.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class DVDController {

    public static DVD newDVD;
    public static ArrayList<DVD> dvdArray = new ArrayList<>();
    public static DVDLibrary newLib = new DVDLibrary(dvdArray);
    public static ConsoleIO c = new ConsoleIO();
    public static ReadWrite rw = new ReadWrite();

    public static void main(String[] args) throws FileNotFoundException {
        boolean playAgain = true;
        int menuChoice;

        try {
            loadOnStart();
        } catch (Exception ex) {
            Logger.getLogger(DVDController.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Welcome to the DVD library!\n");
        do {
            System.out.println("1. Add an DVD\n"
                    + "2. Remove a DVD\n"
                    + "3. List the DVD's\n"
                    + "4. Find number of DVD's with a Title\n"
                    + "5. Find DVD by Title\n"
                    + "6. End");
            menuChoice = c.readInteger("Enter the number of your choice:", 1, 6);

            switch (menuChoice) {
                case 1:
                    addDVD();

                    break;
                case 2:
                    removeDVD();
                    
                    break;
                case 3:
                    viewAll();
                    
                    break;
                case 4:
                    numberOfTitles();
                    
                    break;
                case 5:
                    findDVD();
                    
                    break;
                case 6:
                    playAgain = false;
                    saveOnEnd();
                    
                    break;
                default:
                    playAgain = false;
                    saveOnEnd();
                    
                    break;
            }
        } while (playAgain);

    }

    public static void addDVD() {
        String title,
                year,
                rating,
                dirName,
                runtime,
                userNote;
        title = c.readString("Title: ");
        year = c.readString("Year: ");
        rating = c.readString("Rating: ");
        dirName = c.readString("Director Name: ");
        runtime = c.readString("Runtime: ");
        userNote = c.readString("Note: ");

        newDVD = new DVD(title, year, rating, dirName, userNote, runtime);
        newLib.add(newDVD);
    }

    public static void viewAll() {
        dvdArray = newLib.getAll();
        for (DVD a : dvdArray) {
            int num = dvdArray.indexOf(a);
            System.out.println("\nEntry #: " + (num + 1));
            printDVD(a);
        }

    }

    public static void printDVD(DVD a) {
        System.out.println("");
        System.out.println("Title: " + a.getTitle() + " ");
        System.out.println("Year: " + a.getYear() + " ");
        System.out.println("Rating: " + a.getRating() + " ");
        System.out.println("Director: " + a.getDirName() + ", ");
        System.out.println("Runtime: " + a.getRuntime() + " minutes");
        System.out.println("Note: " + a.getUserNote());
        System.out.println("\n");
    }

    public static void removeDVD() {
        viewAll();
        String str = c.readString("Enter Title of DVD to remove: ");
        newDVD = newLib.find(str);
        if (newDVD == null) {
            System.out.println("We could not find that Title");
        } else {
            newLib.remove(newDVD);
            System.out.println("\nDVD Removed.\n");
        }
    }

    public static void numberOfTitles() {
        String str = c.readString("Enter Title of DVD to search: ");
        newLib.findTitleNum(str);
    }

    public static void findDVD() {
        String str = c.readString("Enter the title of the DVD you are trying to find: ");
        newDVD = newLib.find(str);
        if (newDVD == null) {
            System.out.println("We could not find that Title");
        } else {
            printDVD(newDVD);
        }
    }

    public static void loadOnStart() {

        dvdArray = rw.load("dvdlist.txt");
        if (dvdArray == null) {
            System.out.println("File failed to load");
        } else {
            newLib = new DVDLibrary(dvdArray);
        }
    }

    public static void saveOnEnd() throws FileNotFoundException {

        boolean a = rw.save(dvdArray, "dvdlist.txt");

        if (!a) {
            System.out.println("Unsuccessful save.");
        }
    }
}
