/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dvdlibrary.operations;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DVDLibrary {

    private ArrayList<DVD> library;

    public DVDLibrary(ArrayList library) {
        this.library = library;
    }

    public void add(DVD a) {
        library.add(a);
    }

    public void remove(DVD a) {
        library.remove(a);
    }

    public DVD find(String a) {
        for (DVD d : library) {
            if (d.getTitle().equalsIgnoreCase(a)) {
                return d;
            }
        }
        return null;
    }

    public int findTitleNum(String a) {
        int count = 0;
        for (DVD d : library) {
            if (d.getTitle().equalsIgnoreCase(a)) {
                count++;
            }
        }
        return count;
    }

    public ArrayList<DVD> getAll() {
        ArrayList<DVD> newList = new ArrayList<>();
        newList.addAll(library);
        return newList;
    }

}
