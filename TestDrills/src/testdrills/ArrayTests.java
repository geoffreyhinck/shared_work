/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testdrills;

/**
 *
 * @author apprentice
 */
public class ArrayTests {

    public static void main(String[] args) {
        int[] g = {2, 3, 4, 5};
        System.out.println(sameFirstLast6(g));
    }

    public boolean firstLast6(int[] numbers) {
        int a;
        a = numbers.length - 1;
        if (numbers[0] == 6 || numbers[a] == 6 && numbers.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean sameFirstLast6(int[] numbers) {
        int a;
        a = numbers.length - 1;
        if (numbers[0] == numbers[a] && numbers.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    public int[] makePi(int n) {
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            String piDigit;
            double pi = Math.PI;
            piDigit = Double.toString(pi);

            a[i] = piDigit.charAt(i);
        }
        return a;
    }

    public boolean commonEnd(int[] numbers, int[] num2) {
        int a, b;
        a = numbers.length - 1;
        b = num2.length - 1;
        if (num2[b] == numbers[a] && numbers.length > 0 && num2.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    public int Sum(int[] numbers) {
        int total = 0;
        for (int i = 0; i < numbers.length; i++) {
            total += numbers[i];
        }
        return total;
    }

}
