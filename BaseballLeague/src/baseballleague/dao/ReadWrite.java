/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baseballleague.dao;

import baseballleague.operations.Player;
import baseballleague.operations.Team;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ReadWrite {

    public ArrayList<Team> loadTeam(String fileName) throws FileNotFoundException {
        String currentLine;
        ArrayList<Team> tempLib = new ArrayList<>();

        //String[] splitLine;
        try {
            Scanner file = new Scanner(new FileReader(fileName));

            while (file.hasNextLine()) {
                currentLine = file.nextLine();
//                splitLine = currentLine.split("::");
                Team temp = new Team(currentLine,null);
                temp.setName(currentLine);
                temp.setMyTeam(null);
                tempLib.add(temp);
            }
        } catch (FileNotFoundException e) {
            return null;
        }
        return tempLib;
    }

    public boolean saveTeam(ArrayList<Team> d, String fileName) throws FileNotFoundException {
        try {
            PrintWriter output = new PrintWriter(new FileWriter(fileName));
            String outString;
            Team current;
            Iterator<Team> iter = d.iterator();
            while (iter.hasNext()) {
                current = iter.next();
                outString = current.getName();
                output.println(outString);
                outString = "";
            }
            output.flush();
            output.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public ArrayList<Player> loadPlayers(String fileName) throws FileNotFoundException {
        ArrayList<Player> tempLib = new ArrayList<>();

        String currentLine;
        String[] splitLine;
        try {
            Scanner file = new Scanner(new FileReader(fileName));

            while (file.hasNextLine()) {
                currentLine = file.nextLine();
                splitLine = currentLine.split("::");
                Player temp = new Player(splitLine[0],
                        splitLine[1], parseInt(splitLine[2]));
                tempLib.add(temp);
            }
        } catch (FileNotFoundException e) {
            return null;
        }
        return tempLib;
    }

    public boolean savePlayers(ArrayList<Player> d, String fileName) throws FileNotFoundException {
        try {
            PrintWriter output = new PrintWriter(new FileWriter(fileName));
            String outString;
            Player current;
            Iterator<Player> iter = d.iterator();
            while (iter.hasNext()) {
                current = iter.next();
                outString = current.getName() + "::"
                        + current.getPosition() + "::"
                        + current.getNumber();
                output.println(outString);
                outString = "";
            }
            output.flush();
            output.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

}
