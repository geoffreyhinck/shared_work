/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baseballleague.operations;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class Team {
    
    private ArrayList<Player> myTeam;
    private String name;
    
    public Team(String name,ArrayList<Player> myTeam) {
        this.myTeam = myTeam;
        this.name = name;

        
    }
     
    public void addPlayer(Player p){
        myTeam.add(p);
    }
    
    public void removePlayer(Player p) {
        myTeam.remove(p);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the myTeam
     */
    public ArrayList<Player> getMyTeam() {
        return myTeam;
    }

    /**
     * @param myTeam the myTeam to set
     */
    public void setMyTeam(ArrayList<Player> myTeam) {
        this.myTeam = myTeam;
    }
    
}
