/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baseballleague.operations;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class League {
    
    private ArrayList<Team> myLeague;
    
    public League() {
        //this.myLeague = myLeague;
        myLeague=new ArrayList<>();
    }
    
    public void addTeam(Team t) {
        myLeague.add(t);
    }
    
    public void removeTeam(Team t) {
        myLeague.remove(t);
    }
    
    /**
     * @return the myLeague
     */
    public ArrayList<Team> getMyLeague() {
        return myLeague;
    }

    /**
     * @param myLeague the myLeague to set
     */
    public void setMyLeague(ArrayList<Team> myLeague) {
        this.myLeague = myLeague;
    }
    
    
    
}
