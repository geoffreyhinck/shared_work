/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baseballleague.dto;

import baseballleague.dao.ReadWrite;
import static baseballleague.dto.BBLController.theTeam;
import baseballleague.operations.League;
import baseballleague.operations.Player;
import baseballleague.operations.Team;
import baseballleague.ui.ConsoleIO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class BBLController {

    public static String name;
    public static Player newPlayer;
    public static ArrayList<Player> playerArray = new ArrayList<>();
    public static ArrayList<Team> teamArray = new ArrayList<>();
    public static Team theTeam = new Team(name,playerArray);
    public static League theLge = new League();
    public static ConsoleIO cio = new ConsoleIO();
    public static ReadWrite rw = new ReadWrite();

    public static void main(String[] args) {
        boolean playAgain = true;
        int menuChoice;

        try {
            loadOnStart();
        } catch (Exception ex) {
            Logger.getLogger(BBLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("\nWelcome to the League Manager!\n");
        do {
            cio.write("1. Create a Team\n"
                    + "2. Add a Player (must join a team)\n"
                    + "3. List all Teams\n"
                    + "4. List all Players on a Team\n"
                    + "5. Trade a Player to a different Team\n"
                    + "6. Delete a Player\n"
                    + "7. End\n");

            menuChoice = cio.readInteger("\nEnter the number of your choice:", 1, 7);

            switch (menuChoice) {
                case 1:
                    createTeam();

                    break;
                case 2:
                    addPlayer();

                    break;
                case 3:
                    listTeams();

                    break;
                case 4:
                    listPlayersOnTeam();

                    break;
                case 5:
                    tradePlayer();

                    break;
                case 6:
                    deletePlayer();

                    break;
                case 7:
                    playAgain = false;
                    try {
                        saveOnEnd();
                    } catch (Exception ex) {
                        Logger.getLogger(BBLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                default:
                    playAgain = false;
                    try {
                        saveOnEnd();
                    } catch (Exception ex) {
                        Logger.getLogger(BBLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
            }
        } while (playAgain);
    }

    public static void listTeams() {
        teamArray = theLge.getMyLeague();
        int count = 0;
        for (Team t : teamArray) {
            count++;
            cio.write("\nTeam #" + count + "\n" + t.getName());
        }
             
        cio.write("\n\n");
    }

    public static void createTeam() {
        playerArray=new ArrayList<>();
        String str = cio.readString("Name your new team: ");
        Team thisTeam = new Team(str,playerArray);
        thisTeam.setName(str);
        thisTeam.setMyTeam(playerArray);
        theLge.addTeam(thisTeam);
        cio.write("\nTeam Added.  Now fill it with some players!\n\n");
    }

    public static void addPlayer() {
        String name, position, team;
        int number;
        teamArray=theLge.getMyLeague();
        name = cio.readString("Name: ");
        position = cio.readString("Position: ");
        number = cio.readInteger("Number: ");
        team = cio.readString("Plays for which team?: ");

        newPlayer = new Player(name, position, number);

        for (Team t : teamArray) {
            if (t.getName().equals(team)) {
                t.addPlayer(newPlayer);
                t.setMyTeam(playerArray);
                cio.write("\nPlayer Added!\n");
            }
        }
        
    }

    public static void listPlayersOnTeam() {
        listTeams();
        boolean foundTeam = false;
        String myTeam = cio.readString("For which team should I list players");
        teamArray=theLge.getMyLeague();
        for (Team t : teamArray) {
            if (t.getName().equals(myTeam)) {
                foundTeam = true;
                playerArray = t.getMyTeam();
                int count = 0;
                for (Player p : playerArray) {
                    count++;
                    cio.write("Player #" + count + ": \n\n");
                    cio.write("Name: " + p.getName() + "\n");
                    cio.write("Position: " + p.getPosition() + "\n");
                    cio.write("Number: " + p.getNumber() + "\n\n");
                }
            }

        }
        if (!foundTeam) {
            cio.write("\nI did not find a team with that name.\n");
        }

    }

    public static void tradePlayer() {
        listTeams();
        String tGet = cio.readString("\nFrom which team would you like to get a player?: ");
        String tPut = cio.readString("\nTo which team is he being traded?: ");
        for (Team t : teamArray) {
            if (t.getName().equals(tGet)) {
                playerArray = t.getMyTeam();
                int count = 1;
                for (Player p : playerArray) {
                    cio.write("Player #" + count + ": \n\n");
                    cio.write("Name: " + p.getName() + "\n");
                    cio.write("Position: " + p.getPosition() + "\n");
                    cio.write("Number: " + p.getNumber() + "\n");
                }
                String pGet = cio.readString("\nName of player to be traded?: ");
                ArrayList<Player> tempArray = new ArrayList<>();
                tempArray.addAll(playerArray);
                for (Player p : tempArray) {
                    if (p.getName().equals(pGet)) {
                        t.removePlayer(p);
                        t.setMyTeam(playerArray);
                        ArrayList<Player> playerArray2;
                        for (Team t2 : teamArray) {
                            playerArray2 = t2.getMyTeam();
                            if (t2.getName().equals(tPut)) {
                                t2.addPlayer(p);
                                t2.setMyTeam(playerArray2);
                                cio.write("\nSuccess!  Player traded!\n");
                            }
                        }
                    }
                }
            }
        }
    }

    public static void deletePlayer() {
        boolean playerRemoved = false;
        teamArray = theLge.getMyLeague();
        listTeams();
        String tChoice = cio.readString("\nFrom which team would you like to delete a player?: ");
        for (Team t : teamArray) {
            if (t.getName().equals(tChoice)) {
                playerArray = t.getMyTeam();
                int count = 0;
                for (Player p : playerArray) {
                    count++;
                    cio.write("Player #" + count + ": \n\n");
                    cio.write("Name: " + p.getName() + "\n");
                    cio.write("Position: " + p.getPosition() + "\n");
                    cio.write("Number: " + p.getNumber() + "\n");
                }
                String pChoice = cio.readString("\nWhich player would you like to delete?: ");
                ArrayList<Player> tempArray=new ArrayList<>();
                tempArray.addAll(playerArray);
                for (Player p2 : tempArray) {
                    if (p2.getName().equals(pChoice)) {
                        t.removePlayer(p2);
                        playerRemoved = true;
                        t.setMyTeam(playerArray);
                    }
                }
                
            }
        }
        if (playerRemoved) {
            cio.write("\nPlayer Removed!\n");
        } else {
            cio.write("\nSomething went wrong.  No players removed.");

        }
    }

    public static void loadOnStart() throws Exception {

        teamArray = rw.loadTeam("teamlist.txt");
        if (teamArray == null) {
            cio.write("\nTeam File failed to load");
        } else {
            theLge.setMyLeague(teamArray);
            for (Team t : teamArray) {
                playerArray = rw.loadPlayers(t.getName() + "file.txt");
                if (playerArray == null) {
                    cio.write("\nPlayer file " + t.getName() + " failed to load.");
                } else {
                    t.setMyTeam(playerArray);
                }

            }

        }
    }

    public static void saveOnEnd() throws Exception {
        teamArray = theLge.getMyLeague();
        rw.saveTeam(teamArray, "teamlist.txt");
        for (Team t : teamArray) {
            rw.savePlayers(playerArray, t.getName() + "file");
        }

    }

}
