/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Abstract;

/**
 *
 * @author apprentice
 */
public abstract class Shape {
    private String color;


public Shape(String color) {
    this.color = color;
    
    
}

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }
     
    public abstract double getArea();
    
    public abstract double getPerimeter();
}


