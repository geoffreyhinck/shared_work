/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Abstract;

/**
 *
 * @author apprentice
 */
public class Triangle extends Shape {
        
        private double a;
        private double b;
        private double c;
        
        public Triangle (double a,double b, double c, String color) {
            super (color);
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
    @Override
    public double getArea() {
        return ((a*b)/2);
    }
    
    @Override
    public double getPerimeter() {
        return (a+b+c);
    }
    

    /**
     * @return the a
     */
    public double getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(double a) {
        this.a = a;
    }

    /**
     * @return the b
     */
    public double getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(double b) {
        this.b = b;
    }

    /**
     * @return the c
     */
    public double getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(double c) {
        this.c = c;
    }
        
        
        }

