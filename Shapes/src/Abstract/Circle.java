/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Abstract;

/**
 *
 * @author apprentice
 */
public class Circle extends Shape {
    
    private double radius;
    
    public Circle (double radius,String color) {
        super (color);
        this.radius = radius;
    }

    /**
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    @Override
    public double getArea () {
        return (Math.pow(radius, Math.PI));
    }
    
    @Override
    public double getPerimeter () {
        return (2*Math.PI*radius);
    }
        
}
