/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingmachine.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import vendingmachine.operations.Item;
import vendingmachine.operations.VendingMachine;

/**
 *
 * @author apprentice
 */
public class ReadWrite {

    public ArrayList<Item> load() throws FileNotFoundException {
        ArrayList<Item> tempLib = new ArrayList<>();

        String currentLine;
        String[] splitLine;
        try {
            Scanner file = new Scanner(new FileReader("items.txt"));

            while (file.hasNextLine()) {
                currentLine = file.nextLine();
                splitLine = currentLine.split("::");
                Item temp = new Item(splitLine[0],
                        parseDouble(splitLine[1]), parseInt(splitLine[2]));
                tempLib.add(temp);
            }
        } catch (FileNotFoundException e) {
            // left empty for now because it does it's job solely catching the exception
        }
        
        return tempLib;
    }

    public boolean save(ArrayList<Item> d) throws FileNotFoundException {
        try {
            PrintWriter output = new PrintWriter(new FileWriter("items.txt"));
            String outString;
            Item current;
            Iterator<Item> iter = d.iterator();
            while (iter.hasNext()) {
                current = iter.next();
                outString = current.getName() + "::"
                        + current.getPrice() + "::"
                        + current.getInv();

                output.println(outString);
                outString = "";
            }
            output.flush();
            output.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

}
