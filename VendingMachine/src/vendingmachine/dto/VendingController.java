/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingmachine.dto;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import vendingmachine.dao.ReadWrite;
import vendingmachine.operations.Change;
import vendingmachine.operations.Item;
import vendingmachine.operations.VendingMachine;
import vendingmachine.ui.ConsoleIO;

/**
 *
 * @author Geoff & Chet
 */
public class VendingController {

    public static ArrayList<Item> passedItems;
    public static VendingMachine vender;
    public static boolean again = false,
            getNewItem = false;
    public static String itemChosen,
            getAnother;
    public static Change moneyBack = new Change();
    public static double startMoney;
    public static int machineOption;
    public static ConsoleIO ui = new ConsoleIO();
    public static ReadWrite g = new ReadWrite();

    
    public static void main(String[] args) throws FileNotFoundException, NoSuchElementException {

        passedItems = g.load();

        do {
            loadVendingMenu();

            machineOption = ui.readInteger("Do you want to purchase something from the"
                    + " vending machine? Enter 1 to purchase; 2 to exit.");

            if (machineOption == 2) {
                return;
            } else if (machineOption == 1) {
                insertMoney();
                do {
                    vendItem();

                } while (again);
                quitOrResume();

            }
        } while (getNewItem);
    }

    public static void loadVendingMenu() throws NoSuchElementException {
        // Set the starting amount to zero until money is passed in
        vender = new VendingMachine(passedItems, 0);

        for (Item a : vender.getItems()) {
            System.out.println("Item: " + a.getName() + "\nPrice: " + a.getPrice()
                    + "\n# Remaining: " + a.getInv() + "\n\n");
        }

    }

    public static void insertMoney() throws NoSuchElementException {
        startMoney = ui.readDouble("How much money are you putting into the vending machine?");

        System.out.println("You just entered $ " + startMoney + " into the machine.");
        vender.setStartingAmount(startMoney);
    }

    public static void vendItem() throws FileNotFoundException {
        itemChosen = ui.readString("\nWhat item do you want from the choices above?"
                + " Enter the name: ");
        again = vender.vend(itemChosen);
        if (again == false) {
            moneyBack.makeChange(startMoney, vender.getChosenItem().getPrice());
            g.save(passedItems);
        }
    }

    public static void quitOrResume() {
        getAnother = ui.readString("\nDo you want another item? (yes or no)");

        switch (getAnother) {
            case "yes":
                getNewItem = true;
                break;
            case "no":
                getNewItem = false;
                break;
            default:
                System.out.println("You didn't enter a correct choice. The machine"
                        + " will restart.\n");
                getNewItem = true;
                break;

        }
    }
}
