/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingmachine.operations;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class VendingMachine {

    private ArrayList<Item> items;

    private double startingAmount;

    private Item chosenItem;

    public VendingMachine(ArrayList<Item> items, double startingAmount) {
        this.items = new ArrayList<>(items);
        this.startingAmount = startingAmount;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public double getStartingAmount() {
        return startingAmount;
    }

    public void setStartingAmount(double startingAmount) {
        this.startingAmount = startingAmount;
    }

    public boolean vend(String itemName) {
        boolean agn = false, inList = false;

        for (Item i : items) {

            if (i.getName().equalsIgnoreCase(itemName)) {
                if (i.getInv() >= 1) {
                    inList = true;
                    if (this.startingAmount < i.getPrice()) {
                        System.out.println("You do not have enough for this $" + i.getPrice()
                                + " item, you only put $" + this.startingAmount + " into the machine.");
                        agn = true;
                    } else {
                        int updateInv;
                        updateInv = i.getInv() - 1;
                        i.setInv(updateInv);
                        agn = false;
                        chosenItem = i;

                    }

                } else {
                    System.out.println("There are no more of this items available.");
                }
            }

        }
        if (inList == false) {
            System.out.println("That option is not available on this machine.");
            agn = true;
        }
        return agn;
    }

    public Item getChosenItem() {
        return chosenItem;
    }

    public void setChosenItem(Item chosenItem) {
        this.chosenItem = chosenItem;
    }

}
