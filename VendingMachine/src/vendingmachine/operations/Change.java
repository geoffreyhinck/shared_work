/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendingmachine.operations;

/**
 *
 * @author apprentice
 */
public class Change {
    
    private int quarters;
    private int dimes;
    private int nickels;
    private int pennies;
    
    public Change() {
        
    }
    
    public void makeChange(double startingMoney, double itemPrice) {
        double a = (startingMoney * 100) - (itemPrice * 100);
        double q, d, n, p;
        q = a / 25;
        quarters = (int) q;
        
        if (q > 0) {
            a = a % 25;
        } 
        
        d = a / 10;
        dimes = (int) d;
        
        if (d > 0) {
            a = a % 10;
        }
        
        n = a / 5;
        nickels = (int) n;
        
        if (n > 0) {
            a = a % 5;
        }
        
        p = a;
        pennies = (int) p;
        
        System.out.println("You received " + quarters + " quarters, " + dimes + " dimes, " +
                nickels + " nickels, " + pennies + " pennies as change.");
        
        
    }
    
    
}
