/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccountmanager.operations;

/**
 *
 * @author apprentice
 */
public class SavingsAccount extends Account {

    private String type;

    public SavingsAccount(String acctNum, String routNum, String name, double balance,
            double avlBalance, String type) {
        super(acctNum, routNum, name, balance, avlBalance);
        this.type = type;
    }

    @Override
    public void deposit(double depositAmount) {
        double adjustedBalance = this.balance + depositAmount;
        if (depositAmount > 0) {
            if (depositAmount <= 10000) {
                this.availableBalance += depositAmount;
            } else if (depositAmount > 10000) {
                this.balance += depositAmount;
                this.availableBalance += depositAmount;
            }
        }

    }

    @Override
    public void withdraw(double withdrawAmount) {
        if (withdrawAmount > 0 && withdrawAmount < this.availableBalance) {
            this.availableBalance -= withdrawAmount;
            this.balance -= withdrawAmount;
        }
    }

}
