/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccountmanager.operations;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author Geoffrey Hinck
 */
public class ReadWrite {

    public HashMap<Integer, ArrayList<Account>> load() throws FileNotFoundException {
        HashMap<Integer, ArrayList<Account>> ATM = new HashMap();
        ArrayList<Account> tempLib = new ArrayList<>();

        String currentLine;
        String[] splitLine;

        int pin;

        try {
            Scanner file = new Scanner(new FileReader("map.txt"));

            while (file.hasNextLine()) {
                currentLine = file.nextLine();
                splitLine = currentLine.split("::");

                pin = parseInt(splitLine[0]);

                CheckingAccount newChecking = new CheckingAccount(splitLine[1],
                        splitLine[3], splitLine[4], parseDouble(splitLine[6]),
                        parseDouble(splitLine[7]), splitLine[5]);

                SavingsAccount newSavings = new SavingsAccount(splitLine[2],
                        splitLine[3], splitLine[4], parseDouble(splitLine[9]),
                        parseDouble(splitLine[10]), splitLine[8]);

                tempLib.add(newChecking);
                tempLib.add(newSavings);
                ATM.put(pin, tempLib);
            }
        } catch (FileNotFoundException e) {
            // left empty for now because it does it's job solely catching the exception
        }

        return ATM;
    }

    public boolean save(HashMap<Integer, ArrayList<Account>> newBank) throws FileNotFoundException, IOException {
        Set<Integer> pins = newBank.keySet();
        try {
            PrintWriter output = new PrintWriter(new FileWriter("map.txt"));
            String outString, pin, chkAcct, svgAcct, rout, name, typeChk, typeSave;
            double chkBal, chkAvlBal, svgBal, svgAvlBal;
            Account current;

            for (int i : pins) {
                ArrayList<Account> newMap = new ArrayList<>();
                newMap = newBank.get(i);
            }

            outString = "";

            output.flush();
            output.close();
        } catch (IOException e) {
            return false;
        }

        return true;
    }

}
