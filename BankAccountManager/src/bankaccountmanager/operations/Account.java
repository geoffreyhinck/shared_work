/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccountmanager.operations;

/**
 *
 * @author apprentice
 */
public abstract class Account {

    protected String acctNumber;
    protected String routNumber;
    protected String name;
    protected double balance;
    protected double availableBalance;

    public Account(String acctNumber, String routNumber, String name, double balance, double availableBalance) {
        this.acctNumber = acctNumber;
        this.routNumber = routNumber;
        this.name = name;
        this.balance = balance;
        this.availableBalance = availableBalance;

    }

    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }

    public String getRoutNumber() {
        return routNumber;
    }

    public void setRoutNumber(String routNumber) {
        this.routNumber = routNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(double availableBalance) {
        this.availableBalance = availableBalance;
    }
    
    public abstract void deposit(double a);
    
    public abstract void withdraw(double a);

}
