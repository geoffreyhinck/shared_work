/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccountmanager.operations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author apprentice
 */
public class CheckingAccount extends Account {

    private String type;
    private Calendar cal;
    
     /*DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
     cal  = Calendar.getInstance();
     System.out.println(dateFormat.format(cal.getTime())); */

    public static void main(String[] args) {

    }

    public CheckingAccount(String accountNum, String routNum, String name,
            double balance, double avlBalance, String type) {
        super(accountNum, routNum, name, balance, avlBalance);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void deposit(double depositAmount) {
        String today = cal.getTime().toString();

        if (depositAmount > 0) {
            if (depositAmount <= 10000) {
                this.availableBalance += depositAmount;
                this.balance += depositAmount;
            } else if (depositAmount > 10000) {
                this.balance += depositAmount;
                System.out.println("We have began processing your deposit, it should"
                        + " clear in ten days.  ");
            }
        }
    }

    @Override
    public void withdraw(double withdrawAmount) {

        if (withdrawAmount > 0 && withdrawAmount < this.availableBalance) {
            this.availableBalance -= withdrawAmount;
            this.balance -= withdrawAmount;
        }
    }

}
