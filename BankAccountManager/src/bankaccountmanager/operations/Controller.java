/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccountmanager.operations;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author Geoffrey Hinck
 */
public class Controller {
    
    public static SavingsAccount savings;
    public static CheckingAccount checking;
    public static ReadWrite update = new ReadWrite();
    public static HashMap<Integer, ArrayList<Account>> ATM = new HashMap();
    public static ArrayList<Account> accounts = new ArrayList<>();
    public static Set<Integer> pins;
    public static UI ui = new UI();
    
    public static void main(String[] args) throws FileNotFoundException {
        int enterPin;
        String name, chkNumber, svgNumber, routNumber, type, chooseAcct, cAcctConvert;
        double checkBal, svgBal, checkAvlBal, svgAvlBal, depAmount, withAmount;
        int depositWithdraw;
        boolean noMatch = true;
        ATM = update.load();
        pins = ATM.keySet();
        enterPin = ui.readInteger("Welcome to Java Bank, enter your pin in order to view your"
                + " balances: ");
        do {
            noMatch = false;
            for (int key : pins) {
                if (enterPin == key) {
                    noMatch = false;
                    accounts = ATM.get(key);
                    chooseAcct = ui.readString("Do you want your checking or savings account? "
                            + "(type in checking or savings)");
                    cAcctConvert = chooseAcct.toLowerCase();
                    switch (cAcctConvert) {
                        case "checking":
                            name = accounts.get(0).getName();
                            chkNumber = accounts.get(0).getAcctNumber();
                            routNumber = accounts.get(0).getRoutNumber();
                            checkBal = accounts.get(0).getBalance();
                            checkAvlBal = accounts.get(0).getAvailableBalance();
                            
                            type = "checking";
                            checking = new CheckingAccount(chkNumber, routNumber, name,
                                    checkBal, checkAvlBal, type);
                            depositWithdraw = ui.readInteger("\nYour balance is $"
                                    + checkBal + " With an available balance of $" + checkAvlBal
                                    + " \nDo you want to deposit or withdraw? Enter 1 for"
                                    + " deposit, 2 for withdrawal: ");
                            
                            if (depositWithdraw == 1) {
                                depAmount = ui.readDouble("How much do you want to deposit?");
                                checking.deposit(depAmount);
                            } else if (depositWithdraw == 2) {
                                withAmount = ui.readDouble("How much do you want to withdraw?");
                                checking.withdraw(withAmount);
                            }
                            break;
                        case "savings":
                            name = accounts.get(0).getName();
                            svgNumber = accounts.get(0).getAcctNumber();
                            routNumber = accounts.get(0).getRoutNumber();
                            svgBal = accounts.get(0).getBalance();
                            svgAvlBal = accounts.get(0).getAvailableBalance();
                            type = "savings";
                            savings = new SavingsAccount(svgNumber, routNumber, name,
                                    svgBal, svgAvlBal, type);
                            depositWithdraw = ui.readInteger("\nYour balance is $"
                                    + svgBal + " With an available balance of $" + svgAvlBal
                                    + " \nDo you want to deposit or withdraw? Enter 1 for"
                                    + " deposit, 2 for withdrawal: ");
                            break;
                        
                    }
                    
                } else {
                    noMatch = true;
                }
            }
            if (noMatch) {
                enterPin = ui.readInteger("Try Again, you did not enter a correct pin: ");
            }
        } while (noMatch);

        //method to load checking
        //method to load savings
        //method to 
    }
    
}
