/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import operations.Address;
import operations.AddressBook;
import ui.ConsoleIO;

/**
 *
 * @author apprentice
 */
public class Controller {

    public static Address newAddress;
    public static ArrayList<Address> addressArray = new ArrayList<>();
    public static AddressBook newBook = new AddressBook(addressArray);
    public static ConsoleIO c = new ConsoleIO();

    public static void main(String[] args) {

        boolean playAgain = true;
        int menuChoice = 0;
        String str = "";

        System.out.println("Welcome to Geoff and Chet's Address Book!\n");
        do {
            System.out.println("1. Add an Address\n"
                    + "2. Remove an address in Address book\n"
                    + "3. Returns the number of addresses in Address book\n"
                    + "4. List all addresses in Address book\n"
                    + "5. Find address by last name\n"
                    + "6. End");

            menuChoice = c.readInteger("Enter your choice: ", 1, 6);

            switch (menuChoice) {
                case 1:                         //Add address
                    addAddress();
                    break;

                case 2:                         //Remove address
                    removeAddress();
                    break;
                    
                case 3:                         //Count addresses
                    countAddresses();
                    break;

                case 4:                         //View all
                    viewAll();
                    break;

                case 5:                         //Find address
                    findAddresses();
                    break;

                case 6:                         //End program
                    playAgain = false;
                    break;

            }

        } while (playAgain);

    }

    public static void viewAll() {
        addressArray = newBook.getAll();
        for (Address a : addressArray) {
            int num = addressArray.indexOf(a);
            System.out.println("\nEntry #: " + (num + 1));
            printAddress(a);
        }
    }

    public static void printAddress(Address a) {

        System.out.println("");
        System.out.print(a.getfName() + " ");
        System.out.println(a.getlName());
        System.out.println(a.getStreet() + " ");
        System.out.print(a.getCity() + ", ");
        System.out.print(a.getState() + ", ");
        System.out.print(a.getZip());
        System.out.println("\n");
    }

    public static void addAddress() {
        String fName,
                lName,
                street,
                city,
                state,
                zip;
        fName = c.readString("First Name: ");
        lName = c.readString("Last Name: ");
        street = c.readString("Street: ");
        city = c.readString("City: ");
        state = c.readString("State: ");
        zip = c.readString("ZIP: ");

        newAddress = new Address(fName, lName, street, city, state, zip);
        newBook.add(newAddress);
    }

    public static void removeAddress() {
        
        viewAll();
        String str = c.readString("Enter last name of record to remove: ");
        newAddress = newBook.find(str);
        newBook.remove(newAddress);
        System.out.println("\nRecord Removed.\n");

    }
    
    public static void countAddresses() {
         System.out.println("\nThere are " + newBook.count() + " addresses in the Address Book.\n");
    }
    
    public static void findAddresses() {
        String str = c.readString("Enter the last name you are trying to find: ");
                    newAddress = newBook.find(str);
                    printAddress(newAddress);
    }

}
