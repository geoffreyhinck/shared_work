/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operations;

import java.util.ArrayList;
import ui.ConsoleIO;



/**
 *
 * @author apprentice
 */
public class AddressBook {

    private final ArrayList<Address> addressBook;   

    public AddressBook(ArrayList<Address> addressBook) {
        this.addressBook = addressBook;

    }

    public void add(Address address) {
        addressBook.add(address);

    }

    public void remove(Address b) {
        addressBook.remove(b);
    }

    public Address find(String lName) {
        for (Address a : addressBook) {
            if (a.getlName().compareTo(lName) == 0) {
                return a;
            }

        }
        System.out.println("No name matched.");
        // Need to address error handling
        return null;
    }

    public ArrayList<Address> getAll() {
        ArrayList<Address> returnAddress = new ArrayList<>();
        returnAddress.addAll(addressBook);
        return returnAddress;
    }

    public int count() {
        return addressBook.size();
    }

}

